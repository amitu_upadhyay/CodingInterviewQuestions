package Coding_Blocks_Interview_Questions.FindMajorityElements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class FindMajorityElements {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		int[] arr = new int[n]; 
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
		
		List<Integer> ans = majorityElement(arr);
		
		if (ans.isEmpty()) {
			System.out.println("No Majority Elements");
		} else {
			for (Integer integer : ans) {
				System.out.print(integer+" ");
			}
		}

	}
	
	public static List<Integer> majorityElement(int[] arr) {
		
		int element1 = 0;
		int count1 = 0;
		
		int element2 = 0;
		int count2 = 0;
		
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] == element1) {
				count1++;
			}else if (arr[i] == element2) {
				count2++;
			}else if(count1 == 0) {
				element1 = arr[i];
                count1++;
			}else if (count2 == 0) {
				element2 = arr[i];
                count2++;
			}else {
				count1 --;
				count2 --;
			}
		}
		
		//int[] ans = new int[arr.length];
		count1 = count2 = 0;
		
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] == element1) {
				count1++;
			}else if(arr[i] == element2) {
				count2++;
			}
		}
		
		List<Integer> ans = new ArrayList<Integer>();
		
		if(count1> arr.length/3) {
			ans.add(element1);
		}
		if(count2 > arr.length/3) {
			ans.add(element2);
		}
		return ans;		
	}

}
